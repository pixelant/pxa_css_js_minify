<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Css and JS Compressor',
    'description' => 'Additional Js and Css compressing tool. Based on minify tool.',
    'category' => 'fe',
    'author' => 'Andriy Oprysko',
    'author_email' => '',
    'author_company' => 'andriy.oprysko@resultify.com',
    'state' => 'alpha',
    'createDirs' => '',
    'clearCacheOnLoad' => false,
    'version' => '1.0.6',
    'constraints' => [
        'depends' => [
            'typo3' => '6.2.0-8.7.99'
        ],
        'conflicts' => [],
        'suggests' => []
    ]
];
