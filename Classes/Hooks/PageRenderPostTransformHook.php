<?php

namespace Pixelant\PxaCssJsMinify\Hooks;

use Pixelant\PxaCssJsMinify\Utility\MainUtility;
use Pixelant\PxaCssJsMinify\Utility\MinifierUtility;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class PageRenderPostTransformHook
 * @package Pixelant\T3layoutGustavsbergNew\Hooks
 */
class PageRenderPostTransformHook implements SingletonInterface
{
    /**
     * Minify JS and CSS
     *
     * @param $params
     * @param PageRenderer $pageRenderer
     */
    public function renderPostTransform(
        $params,
        /** @noinspection PhpUnusedParameterInspection */
        PageRenderer $pageRenderer
    ) {
        if (MainUtility::isCssCompressionEnabled()) {
            foreach ($params['cssFiles'] as $cssFileConf) {
                if ($this->isFileCompressionAllowed($cssFileConf['file'])) {
                    MinifierUtility::minify($cssFileConf['file'], MinifierUtility::MINIFY_CSS, true);
                }
            }
            foreach ($params['cssInline'] as &$cssInlineCode) {
                if ($result = MinifierUtility::minify($cssInlineCode['code'], MinifierUtility::MINIFY_CSS)) {
                    $cssInlineCode['code'] = $result;
                }
            }
        }

        if (MainUtility::isJsCompressionEnabled()) {
            $libs = ['jsLibs', 'jsFooterLibs', 'jsFiles', 'jsFooterFiles'];
            foreach ($libs as $lib) {
                foreach ($params[$lib] as $jsLib) {
                    // Check if it is external
                    if ($this->isFileCompressionAllowed($jsLib['file'])) {
                        MinifierUtility::minify($jsLib['file'], MinifierUtility::MINIFY_JS, true);
                    }
                }
            }

            $inlineCode = ['jsInline', 'jsFooterInline'];
            foreach ($inlineCode as $jsInline) {
                foreach ($params[$jsInline] as &$jsInlineCode) {
                    if ($result = MinifierUtility::minify($jsInlineCode['code'], MinifierUtility::MINIFY_JS)) {
                        $jsInlineCode['code'] = $result;
                    }
                }
            }
        }
    }

    /**
     * Check if we can compress this file
     *
     * @param $path
     * @return bool
     */
    protected function isFileCompressionAllowed($path)
    {
        $pathInfo = pathinfo(ltrim($path, '/'));

        // Only temp files are allowed.
        return GeneralUtility::isFirstPartOfStr($pathInfo['dirname'], 'typo3temp')
            && GeneralUtility::inList('css,js', $pathInfo['extension']);
    }
}
