<?php

namespace Pixelant\PxaCssJsMinify\Utility;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017 Andriy Oprysko <andriy@pixelant.se>, Pixelant
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\Cache\Frontend\FrontendInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Utility to work with cache
 */
class CacheUtility
{
    /**
     * @var CacheManager
     */
    protected static $cacheManager;

    /**
     * get cache
     *
     * @param string $key
     * @return mixed
     */
    public static function getCache($key)
    {
        $cacheKey = self::getKey($key);

        /** @var FrontendInterface $cache */
        $cache = self::getCacheManager()->getCache(MainUtility::EXT_KEY);

        if ($cache->has($cacheKey)) {
            return $cache->get($cacheKey);
        }

        return false;
    }

    /**
     * set cache
     *
     * @param mixed $value
     * @param string $key
     * @param int $lifetime
     * @return void
     */
    public static function setCache($key, $value, $lifetime = null)
    {
        $cacheKey = self::getKey($key);

        /** @var FrontendInterface $cache */
        $cache = self::getCacheManager()->getCache(MainUtility::EXT_KEY);
        $cache->set($cacheKey, $value, [], $lifetime);
    }

    /**
     * Get key
     *
     * @param string $string
     * @return string
     */
    public static function getKey($string)
    {
        return hash('sha1', $string);
    }

    /**
     * @return CacheManager
     */
    public static function getCacheManager()
    {
        if (self::$cacheManager === null) {
            self::$cacheManager = GeneralUtility::makeInstance(CacheManager::class);
        }

        return self::$cacheManager;
    }
}
