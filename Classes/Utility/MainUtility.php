<?php

namespace Pixelant\PxaCssJsMinify\Utility;

use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Main utility
 */
class MainUtility
{
    const EXT_KEY = 'pxa_css_js_minify';

    /**
     * Extension configuration
     *
     * @var array
     */
    protected static $extensionConfiguration;

    /**
     * Get extension configuration
     *
     * @return array
     */
    public static function getExtensionConfiguration()
    {
        if (self::$extensionConfiguration === null) {
            self::$extensionConfiguration = is_array(self::getTSFE()->config['config']['tx_pxacssjsminify.']) ?
                self::getTSFE()->config['config']['tx_pxacssjsminify.'] : [];
        }

        return self::$extensionConfiguration;
    }

    /**
     * Check if JS compressing enabled
     *
     * @return bool
     */
    public static function isJsCompressionEnabled()
    {
        return self::isOptionEnabled('compressJs');
    }

    /**
     * Check if CSS compressing enabled
     *
     * @return bool
     */
    public static function isCssCompressionEnabled()
    {
        return self::isOptionEnabled('compressCss');
    }

    /**
     * @return TypoScriptFrontendController
     */
    public static function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }

    /**
     * Check if option is disabled
     *
     * @param $option
     * @return bool
     */
    protected static function isOptionEnabled($option)
    {
        $settings = MainUtility::getExtensionConfiguration();

        return isset($settings[$option]) && ((int)$settings[$option] === 1);
    }
}
