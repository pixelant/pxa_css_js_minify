<?php

namespace Pixelant\PxaCssJsMinify\Utility;

use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class MinifierUtility
 * @package Pixelant\PxaCssJsMinify\Utility
 */
class MinifierUtility
{
    /**
     * css
     */
    const MINIFY_CSS = 1;

    /**
     * js minifier
     */
    const MINIFY_JS = 2;

    /**
     * Minify file - JS or CSS
     *
     * @param string $data Relative file path or clean css
     * @param int
     * @param bool $saveInSameFile If compress content should be saved in same file
     * @return mixed
     */
    public static function minify($data, $minify, $saveInSameFile = false)
    {
        // Could be that composer was not installed
        if (!class_exists('MatthiasMullie\\Minify\\CSS')) {
            if (MainUtility::getTSFE()->beUserLogin) {
                throw new \RuntimeException(
                    'Could not find class - "MatthiasMullie\\Minify\\CSS", run composer install in "pxa_css_js_minify"',
                    1510323547399
                );
            }

            return false;
        }

        $hash = CacheUtility::getKey($data);

        if (($minified = CacheUtility::getCache($hash)) === false) {
            switch ($minify) {
                case self::MINIFY_CSS:
                    /** @var CSS $minifier */
                    $minifier = GeneralUtility::makeInstance(CSS::class);
                    break;
                case self::MINIFY_JS:
                    /** @var JS $minifier */
                    $minifier = GeneralUtility::makeInstance(JS::class);
                    break;
                default:
                    /** @var CSS $minifier */
                    $minifier = GeneralUtility::makeInstance(CSS::class);
            }

            $filePath = GeneralUtility::getFileAbsFileName($data);

            $minifier->add(is_file($filePath) ? $filePath : $data);
            $minified = $minifier->minify($saveInSameFile === true ? $filePath : null);

            // save cache
            CacheUtility::setCache($hash, $minified);
        }

        return $minified;
    }
}
