<?php

namespace Pixelant\PxaCssJsMinify\Tests\Unit\Hooks;

use PHPUnit\Framework\MockObject\MockObject;
use Pixelant\PxaCssJsMinify\Hooks\PageRenderPostTransformHook;
use TYPO3\TestingFramework\Core\BaseTestCase;

/**
 * Class PageRenderPostTransformHookTest
 * @package Pixelant\PxaCssJsMinify\Tests\Unit\Hooks
 */
class PageRenderPostTransformHookTest extends BaseTestCase
{
    /**
     * @var PageRenderPostTransformHook|MockObject
     */
    protected $subject = null;

    public function setUp()
    {
        $this->subject = $this->getAccessibleMock(PageRenderPostTransformHook::class, ['renderPostTransform']);
    }

    /**
     * @test
     */
    public function ifFileIsNotTempMinifyNotAllowed()
    {
        $path = 'fileadmin/test/file.css';

        $this->assertFalse($this->subject->_call('isFileCompressionAllowed', $path));
    }

    /**
     * @test
     */
    public function ifFileIsTempMinifyAllowed()
    {
        $path = '/typo3temp/assets/test/file.css';

        $this->assertTrue($this->subject->_call('isFileCompressionAllowed', $path));
    }

    /**
     * @test
     */
    public function onlyCssAndJsFilesExtensionIsAllowed()
    {
        $path = '/typo3temp/assets/test/file.css';
        $path1 = '/typo3temp/assets/test/file.js';
        $path2 = '/typo3temp/assets/test/file.css.gzip';
        $path3 = '/typo3temp/assets/test/file.css.gzip';


        $this->assertTrue($this->subject->_call('isFileCompressionAllowed', $path));
        $this->assertTrue($this->subject->_call('isFileCompressionAllowed', $path1));

        $this->assertFalse($this->subject->_call('isFileCompressionAllowed', $path2));
        $this->assertFalse($this->subject->_call('isFileCompressionAllowed', $path3));
    }
}
