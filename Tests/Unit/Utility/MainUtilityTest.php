<?php
namespace Pixelant\PxaCssJsMinify\Tests\Utility\Unit;

use PHPUnit\Framework\TestCase;
use Pixelant\PxaCssJsMinify\Utility\MainUtility;

/**
 * Class MainUtilityTest
 * @package Pixelant\PxaCssJsMinify\Tests\Unit
 */
class MainUtilityTest extends TestCase
{
    public function setUp()
    {
        $tsfe = new \stdClass();
        $tsfe->config['config']['tx_pxacssjsminify.'] = [
            'compressCss' => '1',
            'compressJs' => '1'
        ];
        $GLOBALS['TSFE'] = $tsfe;
    }

    /**
     * @test
     */
    public function getExtensionConfigurationReturnConfiguration()
    {
        $expect = [
            'compressCss' => '1',
            'compressJs' => '1'
        ];

        $this->assertEquals(
            $expect,
            MainUtility::getExtensionConfiguration()
        );
    }

    /**
     * @test
     */
    public function ifOptionsEnabledReturnTrue()
    {
        $this->assertTrue(MainUtility::isCssCompressionEnabled());
        $this->assertTrue(MainUtility::isJsCompressionEnabled());
    }

    /**
     * @test
     */
    public function ifOptionsDisabledReturnFalse()
    {
        $GLOBALS['TSFE']->config['config']['tx_pxacssjsminify.'] = [
            'compressCss' => '0',
            'compressJs' => '0'
        ];

        // Reset settings
        $reflection = new \ReflectionProperty(MainUtility::class, 'extensionConfiguration');
        $reflection->setAccessible(true);
        $reflection->setValue(null, null);

        $this->assertFalse(MainUtility::isCssCompressionEnabled());
        $this->assertFalse(MainUtility::isJsCompressionEnabled());
    }

    /**
     * @test
     */
    public function getConfigurationWithNoSettingsReturnEmptyArray()
    {
        $GLOBALS['TSFE']->config['config'] = [];

        // Reset settings
        $reflection = new \ReflectionProperty(MainUtility::class, 'extensionConfiguration');
        $reflection->setAccessible(true);
        $reflection->setValue(null, null);

        $conf = MainUtility::getExtensionConfiguration();
        $isArray = is_array($conf);

        $this->assertTrue($isArray);
        $this->assertEmpty($conf);
    }

    public function tearDown()
    {
        unset($GLOBALS['TSFE']);
    }
}
