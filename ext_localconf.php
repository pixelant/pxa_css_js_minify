<?php
defined('TYPO3_MODE') || die('Access denied.');

// Cache for one month
if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$_EXTKEY])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][$_EXTKEY] = [
        'frontend' => \TYPO3\CMS\Core\Cache\Frontend\StringFrontend::class,
        'backend' => \TYPO3\CMS\Core\Cache\Backend\FileBackend::class,
        'options' => [
            'defaultLifetime' => 3600 * 24 * 7 * 30
        ]
    ];
}

if (TYPO3_MODE === 'FE') {
    // Register hook to minify CSS and JS
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-postTransform'][$_EXTKEY] =
        \Pixelant\PxaCssJsMinify\Hooks\PageRenderPostTransformHook::class . '->renderPostTransform';
}

// Include autoloader, if not in composer mode
// @codingStandardsIgnoreStart
if ((!defined('TYPO3_COMPOSER_MODE') || TYPO3_COMPOSER_MODE !== true)
    && file_exists(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_css_js_minify') . 'vendor/autoload.php')
) {
    require_once \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('pxa_css_js_minify') . 'vendor/autoload.php';
}
// @codingStandardsIgnoreEnd
