# JS and CSS minifier
## Extension provides additional compression for Css and Js files based on [minify tool](https://github.com/matthiasmullie/minify).  

### Requirements
* Composer
* TYPO3 Composer mode
* If TYPO3 is not in composer mode, you will need to go to extension folder and run "composer install" manually.  

### How to use
* Install extension.
* Enable compression in TypoScript settings

```typo3_typoscript
config {
    tx_pxacssjsminify  {
        compressCss = 1
        compressJs = 1
    }
}
```